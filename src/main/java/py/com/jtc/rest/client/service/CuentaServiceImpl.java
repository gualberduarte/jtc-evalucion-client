package py.com.jtc.rest.client.service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import py.com.jtc.rest.client.bean.Cuenta;

@Service
public class CuentaServiceImpl  implements CuentaService{
	private static List<Cuenta> cuentas;
	public static final String URL_API = "http://localhost:9090/rest/cuentas/";
	
	@Override
	public List<Cuenta> obtenerCuentas() {
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Cuenta[]> responseEntity = restTemplate.getForEntity(URL_API, Cuenta[].class);

		cuentas = Arrays.asList(responseEntity.getBody());
		return cuentas;
	}
	
	@Override
	public Cuenta obtenerCuenta(Integer id) {
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Cuenta> responseEntity = restTemplate.getForEntity(URL_API+"{id}", Cuenta.class);

		Cuenta cuenta = responseEntity.getBody();
		return cuenta;
	}

	@Override
	public void insertarCuenta(Cuenta cuenta) {
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Cuenta> responseEntity = restTemplate.postForEntity(URL_API, cuenta, Cuenta.class);
		
	}
	
	@Override
	public Cuenta editarCuenta(Integer id, Cuenta cuenta) {
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.put(URL_API+"{id}", cuenta, id);

		return cuenta;
	}
	
	@Override
	public void eliminarCuenta(Integer id) {
		Map<String, Integer> params = new HashMap<String, Integer>();
	    params.put("id",id);
	    RestTemplate restTemplate = new RestTemplate();
	    restTemplate.delete(URL_API + "{id}",params);
	    
	}

}