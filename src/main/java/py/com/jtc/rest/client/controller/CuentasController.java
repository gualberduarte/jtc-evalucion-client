package py.com.jtc.rest.client.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import py.com.jtc.rest.client.bean.Cuenta;
import py.com.jtc.rest.client.service.CuentaService;

@Controller
@RequestMapping("/rest-client")
public class CuentasController {
	
	@Autowired
	private CuentaService CuentaService;

	@GetMapping("/cuentas")
	public String listaCuentas(Model model) { 
		System.out.println("cuentas");
		model.addAttribute("cuentas", CuentaService.obtenerCuentas());
		return "cuentas";
	}
	
	@GetMapping("/cuentas/add")
	public String cuentasForm(Model model) {
		model.addAttribute("cuenta", new Cuenta());
		return "cuentas";
	}
	
	@GetMapping("/cuentas/edit/{id}")
	public String cuentasEdit(@PathVariable("id") Integer id, Model model) {
		model.addAttribute("cuenta", CuentaService.obtenerCuenta(id));
		return "cuentas";
	}
	
	@PostMapping("/cuentas")
	public String agregarCuenta(@ModelAttribute("cuenta") Cuenta cuenta) {
		CuentaService.insertarCuenta(cuenta);
		if (cuenta == null) {
			
		}
		
		return "redirect:/rest-client/cuentas";
	}
	
	@GetMapping("/cuentas/delete/{id}")
	public String deleteCuenta(@PathVariable("id") int id) {
		CuentaService.eliminarCuenta(id);
		return "redirect:/rest-client/cuentas";
	}

}

